#include<iostream>
#include <ctime>    // For time()
#include <cstdlib>  // For srand() and rand()

using namespace std;

int main()
{
	
	int TargetNumber; // int for the random number they have to find
	srand(time(0));  // Initialize random number generator.
	TargetNumber = (rand() % 64) + 1; // makes the number random
	
	///////////
	///HUMAN///
	///////////
	
	int HumanTargetGuess = 0; // sets the human guess number 
	int HumanHighNum = 64; // sets the high number that can be guessed
	int HumanlowNum = 1; // sets the low number 
	bool HGuessCorrect = false; // sets up the variable for the do while
	int HGuessAmount = 0; // int for checking the amount the do while is used
	
	cout << "Welcome to Find'n Gold!\nYou are going to find gold in different ways!\n";
	cout << "Gold is in one grid spot in a grid of " << HumanHighNum << " plots\n";
	system("pause"); // pauses console
	system("CLS"); // Clears console
	do
	{
		HGuessAmount++; // adds to the amount that the do while has been done
		cout << "At what number do you try and pan?\n";
		cin >> HumanTargetGuess;
		
		if(HumanTargetGuess == TargetNumber) // checks if the right number is guessed
		{
			cout << "Good go'n partner ya found the gold\n";
			system("pause");// pauses console
			system("CLS");// Clears console
			HGuessCorrect = true; // sets the while condition to true
		}
		else
		{
			cout << "DAD GUMMIT sorry partner, you didn't find not'n\n";
			if(HumanTargetGuess > TargetNumber)// checks if the guess is higher than the real number
			{
				cout << "Ya too high on your guess\n";
			}
			else if (HumanTargetGuess < TargetNumber) // checks if the guess is lower than the real number
			{
				cout << "Ya too low on your guess\n";
			}
		}
	}while (HGuessCorrect == false);//while
	system("CLS");// Clears console

	////////////
	///LINEAR///
	////////////

	int LinearTargetGuess = 0;// sets the Linear guess number 
	int LinearHighNum = 64;// sets the high number that can be guessed
	int LinearlowNum = 1;// sets the low number 
	bool LGuessCorrect = false;// sets up the variable for the do while
	int LGuessAmount = 0;// int for checking the amount the do while is used
	
	cout << "Yer buddy over yonder has just been go'n in order and see'n if he finds it.\nLets see how he's do'n\n";
	system("pause");// pauses console
	do
	{
		LGuessAmount++; // adds to the amount that the do while has been done
		LinearTargetGuess++;
		cout << "He's try'n plot " << LinearTargetGuess <<".\n";
		
		if(LinearTargetGuess == TargetNumber)// checks if the right number is guessed
		{
			cout << "He got it! GO HIM!\n";
			LGuessCorrect = true;// sets the while condition to true
			system("pause");// pauses console
		}
	}while (LGuessCorrect == false);//while
	system("CLS");// Clears console
	
	////////////
	///RANDOM///
	////////////

	bool RGuessCorrect = false;// sets up the variable for the do while
	int Random; //int for the random
	int RGuessAmount = 0;// int for checking the amount the do while is used
	
	cout << "This here is Skeeter... He's been here for days...\nHe's been try'n randomly and even going back on his own plots...\n";
	system("pause");// pauses console
	do
	{
		RGuessAmount++; // adds to the amount that the do while has been done
		Random = rand() % 64 + 1;
		cout << "He's try'n " << Random << "\n";
		

		
		if(Random == TargetNumber)// checks if the right number is guessed
		{
			cout << "Wow he actually did it... \nHe's been here for so long...\n";
			system("pause");// pauses console
			RGuessCorrect = true;// sets the while condition to true
		}
		else
		{
			cout << "Nope he didn't get it again\n";
		}
	}while (RGuessCorrect == false);//while
	system("CLS");// Clears console

	////////////
	///BINARY///
	////////////

	int BinaryTargetGuess;// sets the Binary guess number 
	int BinaryHighNum = 64;// sets the high number that can be guessed
	int BinarylowNum = 1;// sets the low number 
	bool BGuessCorrect = false;// sets up the variable for the do while
	int BGuessAmount = 0;// int for checking the amount the do while is used
	
	cout << "This is one of them new fangled machines that can cacalate where it mines and figure it out\n";
	system("pause");// pauses console
	do 
	{
		BGuessAmount++; // adds to the amount that the do while has been done
		BinaryTargetGuess = (BinaryHighNum - BinarylowNum) / 2 + BinarylowNum; // math for the 
		cout << "It's gonna try: "<< BinaryTargetGuess << "\n";
		
		if(BinaryTargetGuess == TargetNumber)// checks if the right number is guessed
		{
			cout << "It did it! That's cool but it's still a machine...\n";
			system("pause");// pauses console
			BGuessCorrect = true;// sets the while condition to true
		}
		else 
		{
			if (TargetNumber < BinaryTargetGuess)// checks if the guess is lower than the real number
			{
				BinaryHighNum = BinaryTargetGuess;
				cout << "Its too low\n";
			}
			if (TargetNumber > BinaryTargetGuess)// checks if the guess is higher than the real number
			{
				BinarylowNum = BinaryTargetGuess;
				cout << "Its too high\n";
			}
		}
	} while (BGuessCorrect == false); //while
	
	system("CLS");// Clears console
	cout << "You searched " << HGuessAmount << " times" << endl;
	cout << "Your friend tried " << LGuessAmount << " times" << endl;
	cout << "Skeeter searched " << RGuessAmount << " times" << endl;
	cout << "The silly computer tried " << BGuessAmount << " times" << endl;
	system("pause"); // pauses console
}